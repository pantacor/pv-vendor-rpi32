
# Bootloader building instructions:
1. Clone repository **pv-uboot-rpi**:
```
git clone https://gitlab.com/pantacor/pv-uboot-rpi
cd pv-uboot-rpi
```
2. Create the defconfig for the target. Make sure to set `CROSS_COMPILE` to a valid arm32 cross-compiler. Replace DEFCONFIG with the defconfig of your Raspberry PI target:
```
make ARCH=arm CROSS_COMPILE=../../prebuilt/arm-linux-musleabihf/bin/arm-linux-musleabihf- DEFCONFIG_NAME
```
| RPI Version | DEFCONFIG_NAME      |
| ----------- | ------------------- |
| RPi 2       | rpi_2_defconfig     |
| RPi 3       | rpi_3_32b_defconfig |
| RPi 4       | rpi_4_32b_defconfig |



3. Build U-Boot binary `u-boot.bin`:
```
make CROSS_COMPILE=~/Pantacor/pantavisor/prebuilt/arm-linux-musleabihf/bin/arm-linux-musleabihf- ARCH=arm -j 12
```


# Note:
Bootloader binaries in this directory were built using the commit [42be22dda2dd8c76c671861f4a0224afc8b40710](https://gitlab.com/pantacor/pv-uboot-rpi/-/commit/42be22dda2dd8c76c671861f4a0224afc8b40710).